from enum import auto

from src.models.extended_enum import ExtendedEnum


class WebDriverType(ExtendedEnum):
    CHROME = auto()
    FIREFOX = auto()


class PriorityType(ExtendedEnum):
    CRITICAL = "Krytyczny"
    IMPORTANT = "Ważny"
    NOT_IMPORTANT = "Mało ważny"
    TRIVIAL = "Trywialny"
