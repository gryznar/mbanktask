from src.models.locator import Locator
from src.models.page import Page


class LoginPageLocators:
    EMAIL_ADDRESS_INPUT = Locator.id("email")
    PASSWORD_INPUT = Locator.id("password")
    LOGIN_BUTTON = Locator.id("login")
    LOGIN_ERROR = Locator.class_name("login_form_error")


class LoginPage(Page):
    URL = "http://demo.testarena.pl/zaloguj"

    @property
    def locators(self):
        return LoginPageLocators()

    def fill_email_address_input(self, email):
        self.send_keys(email, self.locators.EMAIL_ADDRESS_INPUT)

    def fill_password_input(self, password):
        self.send_keys(password, self.locators.PASSWORD_INPUT)

    def click_login_button(self):
        self.click(self.locators.LOGIN_BUTTON)

    def login(self, email, password):
        self.fill_email_address_input(email)
        self.fill_password_input(password)
        self.click_login_button()

    def get_login_form_error(self):
        return self.get_text(self.locators.LOGIN_ERROR)
