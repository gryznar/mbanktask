from src.models.locator import Locator
from src.models.page import Page


class HeaderLocators:
    USER_INFO = Locator.class_name("user-info")
    USER_EMAIL = Locator.xpath('//span[@class="user-info"]/small')
    PROJECT_BUTTON = Locator.id("activeProject_chosen")
    PROJECTS_SEARCH_INPUT = Locator.xpath('//div[@id="activeProject_chosen"]//input')
    LOGOUT_BUTTON = Locator.class_name("header_logout")

    @staticmethod
    def project_on_projects_list(project):
        return Locator.xpath(
            f'//div[@id="activeProject_chosen"]//ul//em[text()="{project}"]'
        )


class Header(Page):
    @property
    def locators(self):
        return HeaderLocators()

    def is_user_info_visible(self):
        return self.is_element_visible(self.locators.USER_INFO)

    def get_user_email(self):
        return self.get_text(self.locators.USER_EMAIL)

    def select_project(self, project_name: str):
        self.click(self.locators.PROJECT_BUTTON)
        self.send_keys(project_name, self.locators.PROJECTS_SEARCH_INPUT)
        self.click(self.locators.project_on_projects_list(project_name))

    def click_on_logout_button(self):
        self.click(self.locators.LOGOUT_BUTTON)


class MenuLocators:
    TASKS_TAB = Locator.xpath('//span[@class="icon_tool icon-20"]/..')


class Menu(Page):
    @property
    def locators(self):
        return MenuLocators()

    def click_on_tasks_tab(self):
        self.click(self.locators.TASKS_TAB)


class TestArenaPageLocators:
    header = HeaderLocators()
    menu = Menu()


class TestArenaPage(Page):
    @property
    def locators(self):
        return TestArenaPageLocators()

    @property
    def header(self):
        return Header(self.driver)

    @property
    def menu(self):
        return Menu(self.driver)
