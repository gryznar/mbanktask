from src.models.locator import Locator
from src.pages.test_arena.app.page import TestArenaPage


class TasksPageLocators:
    ADD_TASK_BUTTON = Locator.xpath(
        '//a[@class="button_link" and contains(@href, "task_add")]'
    )
    SEARCH_INPUT = Locator.id("search")
    SEARCH_BUTTON = Locator.id("j_searchButton")

    @staticmethod
    def task_details(title):
        return Locator.xpath(
            f'//td[@class="title_task_max"]/a[normalize-space(text())="{title}"]'
        )


class TasksPage(TestArenaPage):
    @property
    def locators(self):
        return TasksPageLocators()

    def click_on_add_task_button(self):
        self.click(self.locators.ADD_TASK_BUTTON)

    def search(self, phrase):
        self.send_keys(phrase, self.locators.SEARCH_INPUT)
        self.click(self.locators.SEARCH_BUTTON)

    def is_task_visible(self, title):
        return self.is_element_visible(self.locators.task_details(title))
