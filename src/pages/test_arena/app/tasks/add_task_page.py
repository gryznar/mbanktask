from dataclasses import dataclass
from datetime import datetime
from time import sleep
from typing import Literal

from selenium.webdriver.common.keys import Keys

from src.data.enums import PriorityType
from src.models.locator import Locator
from src.models.page import LocatorType
from src.pages.test_arena.app.page import TestArenaPage


@dataclass
class Task:
    title: str
    description: str
    release_name: str
    environments: list[str]
    versions: list[str]
    priority: PriorityType
    due_date: datetime
    user_assigned: str or Literal["me"]
    tags: list[str]


class AddTaskPageLocators:
    TITLE_INPUT = Locator.id("title")
    DESCRIPTION_TEXT_ARENA = Locator.id("description")
    RELEASE_INPUT = Locator.id("releaseName")
    ENVIRONMENTS_INPUT = Locator.id("token-input-environments")
    VERSIONS_INPUT = Locator.id("token-input-versions")
    PRIORITY_SELECT = Locator.id("priority")
    DUE_DATE_INPUT = Locator.id("dueDate")
    DATE_TIME_PICKER = Locator.id("ui-datepicker-div")
    ASSIGN_TO_ME_BUTTON = Locator.id("j_assignToMe")
    TAGS_INPUT = Locator.id("token-input-tags")
    SAVE_BUTTON = Locator.id("save")
    TASK_WAS_ADDED_ALERT = Locator.xpath('//div[@id="j_info_box"]//p[text()="Zadanie zostało dodane."]')
    ALERT_CLOSE_BUTTON = Locator.class_name("j_close_button")
    BACK_BUTTON = Locator.xpath('//a[@class="button_link"  and contains(@href, "tasks")]')


class AddTaskPage(TestArenaPage):
    @property
    def locators(self):
        return AddTaskPageLocators()

    def fill_title_input(self, title):
        self.send_keys(title, self.locators.TITLE_INPUT)

    def fill_description_text_arena(self, description):
        self.send_keys(description, self.locators.DESCRIPTION_TEXT_ARENA)

    def fill_release_name_input(self, release_name):
        self.send_keys(release_name, self.locators.RELEASE_INPUT)

    def choose_environments(self, *environments: str):
        self.__choose_options(self.locators.ENVIRONMENTS_INPUT, *environments)

    def choose_versions(self, *versions):
        self.__choose_options(self.locators.VERSIONS_INPUT, *versions)

    def select_priority(self, priority: PriorityType):
        self.select_by_attribute_value(priority.value, "label", self.locators.PRIORITY_SELECT)

    def choose_due_date(self, date: datetime):
        self.send_keys(date.strftime("%Y-%m-%d %H:%M"), self.locators.DUE_DATE_INPUT)
        self.wait_until_visibility_of_element_located(self.locators.DATE_TIME_PICKER)
        self.send_keys(Keys.ESCAPE, Locator.tag_name("html"), clear=False)

    def click_assign_to_me_button(self):
        self.click(self.locators.ASSIGN_TO_ME_BUTTON)

    def assign_to(self, user: str or Literal["me"]):
        if user == "me":
            self.click_assign_to_me_button()

    def choose_tags(self, *tags: str):
        self.__choose_options(self.locators.TAGS_INPUT, *tags)

    def __choose_options(self, locator: LocatorType, *options: str):
        # todo: implement more reliable solution
        for option in options:
            self.send_keys(f"{option}\n", locator)
            sleep(1)
            self.send_keys("\n", locator)

    def click_save_button(self):
        self.click(self.locators.SAVE_BUTTON)

    def add_task(self, task: Task):
        self.fill_title_input(task.title)
        self.fill_description_text_arena(task.description)
        self.fill_release_name_input(task.release_name)
        self.choose_environments(*task.environments)
        self.choose_versions(*task.versions)
        self.select_priority(task.priority)
        self.choose_due_date(task.due_date)
        self.assign_to(task.user_assigned)
        self.choose_tags(*task.tags)
        self.click_save_button()

    def is_alert_about_adding_task_visible(self):
        return self.is_element_visible(self.locators.TASK_WAS_ADDED_ALERT)

    def close_alert(self):
        self.click(self.locators.ALERT_CLOSE_BUTTON)

    def click_back_button(self):
        self.click(self.locators.BACK_BUTTON)
