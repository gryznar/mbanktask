from src.models.locator import Locator
from src.models.page import Page


class DemoPageLocators:
    DEMO_BUTTON = Locator.xpath('//a[@href="http://demo.testarena.pl/"]')


class DemoPage(Page):
    URL = "http://testarena.pl/demo"

    @property
    def locators(self):
        return DemoPageLocators()

    def navigate(self):
        self.navigate_to(self.URL)

    def click_on_demo_button(self):
        self.click(self.locators.DEMO_BUTTON)
