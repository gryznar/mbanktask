from pathlib import Path

from webdriver_manager.chrome import ChromeDriverManager

from src.utils.paths import join_path


class Paths:
    ROOT = Path(__file__).parent.parent
    TESTS_DIR = join_path(ROOT, "tests")
    # TEST ARENA
    TEST_ARENA_TESTS_DIR = join_path(TESTS_DIR, "test_arena")
    TEST_ARENA_TESTS_ACCOUNTS = join_path(TEST_ARENA_TESTS_DIR, "accounts.yaml")

    @property
    def chrome_driver_latest(self):
        return ChromeDriverManager().install()
