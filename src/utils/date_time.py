from datetime import datetime


def get_hex_from_time_now():
    return hex(int(datetime.timestamp(datetime.now()) * 1000))
