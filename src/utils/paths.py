import os


def join_path(*parts):
    return os.path.join(*parts)


def exists_path(path):
    return os.path.exists(path)
