import yaml


def read_yaml(path):
    with open(path) as f:
        return yaml.load(f.read(), Loader=yaml.FullLoader)
