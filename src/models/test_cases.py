from unittest import TestCase

from selenium.webdriver import Remote

from src.models.driver_container import WebDriverContainer
from src.models.page import Page
from tests.test_arena.accounts import Accounts


class WebTestCase(TestCase):
    @property
    def driver(self) -> Remote:
        return WebDriverContainer().get()

    @property
    def page(self) -> Page:
        return Page()


class TestArenaTestCase(WebTestCase):
    @property
    def accounts(self):
        return Accounts()
