from selenium.webdriver import Remote


class WebDriverContainer:
    __driver = None

    @classmethod
    def insert(cls, driver: Remote):
        cls.__driver = driver

    @classmethod
    def get(cls) -> Remote:
        return cls.__driver
