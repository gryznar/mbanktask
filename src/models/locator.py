from selenium.webdriver.common.by import By


class Locator:
    @staticmethod
    def id(value: str):
        return By.ID, value

    @staticmethod
    def name(value: str):
        return By.NAME, value

    @staticmethod
    def class_name(value: str):
        return By.CLASS_NAME, value

    @staticmethod
    def tag_name(value: str):
        return By.TAG_NAME, value

    @staticmethod
    def xpath(value: str):
        return By.XPATH, value
