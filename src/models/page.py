from typing import Callable, Optional

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import Remote
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from src.models.driver_container import WebDriverContainer

LocatorType = tuple[str, str]


class Page:
    DEFAULT_TIMEOUT = 10

    def locators(self):
        """
        Abstract like method for inheritance
        :return:
        """
        pass

    def __init__(self, driver: Remote = None):
        self.driver = driver or WebDriverContainer().get()

    def refresh(self):
        self.driver.refresh()

    def navigate_to(self, url):
        self.driver.get(url)

    def wait_until_presence_of_all_elements_located(
            self, locator: LocatorType, timeout: float = None
    ) -> list[WebElement]:
        return self.__wait_until(
            expected_conditions.presence_of_all_elements_located(locator),
            message=f"presence of all elements located {locator}",
            timeout=timeout,
        )

    def wait_until_visibility_of_any_elements_located(
            self, locator: LocatorType, timeout: float = None
    ) -> list[WebElement]:
        return self.__wait_until(
            expected_conditions.visibility_of_any_elements_located(locator),
            message=f"visibility of any elements located {locator}",
            timeout=timeout
        )

    def wait_until_visibility_of_element_located(
            self, locator: LocatorType, timeout: float = None
    ) -> WebElement:
        return self.__wait_until(
            expected_conditions.visibility_of_element_located(locator),
            message=f"visibility of element located {locator}",
            timeout=timeout
        )

    # localizing
    def find_elements(
            self,
            locator: LocatorType,
            visible: bool = True,
            enabled: bool = True,
            timeout: float = None,
            raise_timeout: bool = True
    ):
        try:
            if visible:
                visible_elements = self.wait_until_visibility_of_any_elements_located(locator, timeout)
                return (
                    self.__filter_enabled_elements(visible_elements)
                    if enabled
                    else visible_elements
                )
            else:
                elements = self.wait_until_presence_of_all_elements_located(locator, timeout)
                return self.__filter_enabled_elements(elements) if enabled else elements
        except TimeoutException:
            if not raise_timeout:
                return []
            raise

    def find_element(
            self,
            locator: LocatorType,
            visible: bool = True,
            enabled: bool = True,
            timeout: float = None,
            raise_timeout: bool = True
    ):
        elements = self.find_elements(
            locator, visible=visible, enabled=enabled, timeout=timeout, raise_timeout=raise_timeout
        )
        return elements[0] if elements else None

    # actions
    def click(self, locator: LocatorType, timeout: float = None):
        self.find_element(locator, timeout=timeout).click()

    def click_via_js(self, locator: LocatorType, timeout: float = None):
        element = self.find_element(locator, visible=False, enabled=False, timeout=timeout)
        self.driver.execute_script("arguments[0].click();", element)

    def send_keys(self, keys: str, locator: LocatorType, clear: bool = True, timeout: float = None):
        element = self.find_element(locator, timeout=timeout)
        if clear:
            element.clear()
        element.send_keys(keys)

    def select_by_attribute_value(self, value, attribute: str, locator: LocatorType, timeout: float = None):
        element = self.find_element(locator, timeout=timeout)
        element.find_element_by_xpath(f'./option[normalize-space(@{attribute})="{value}"]').click()

    # getting values
    def get_text(self, locator: LocatorType, raise_timeout: bool = True):
        if element := self.find_element(locator, raise_timeout=raise_timeout):
            return element.text

    # verifying
    class __Decorators:
        @staticmethod
        def verify(func):
            def wrapper(*args, **kwargs):
                try:
                    func(*args, **kwargs)
                    return True
                except TimeoutException:
                    return False

            return wrapper

    @__Decorators.verify
    def is_element_visible(self, locator: LocatorType, timeout: float = None):
        return self.find_element(locator, visible=True, timeout=timeout)

    # urls
    def wait_until_url_changes(self, url, timeout: float = None):
        self.__wait_until(
            expected_conditions.url_changes(url),
            message="url changes",
            timeout=timeout
        )

    def wait_until_url_to_be(self, url: str, timeout: float = None):
        self.__wait_until(
            lambda _: self.driver.current_url.removesuffix("/") == url.removesuffix("/"),
            message="url to be",
            timeout=timeout
        )

    # windows
    def switch_to_next_window(self, timeout: float = None):
        current_window_handle = self.driver.current_window_handle
        current_handle_index = self.driver.window_handles.index(current_window_handle)
        self.__wait_until(
            lambda _: self.driver.window_handles[-1] != current_window_handle,
            message="window to switch on appear",
            timeout=timeout
        )
        self.driver.switch_to.window(self.driver.window_handles[current_handle_index + 1])

    def __wait_until(
            self,
            method: Callable,
            message: str,
            timeout: Optional[float],
    ):
        return self.__get_web_driver_wait(timeout).until(method, f"unfulfilled wait until {message}")

    def __get_web_driver_wait(self, timeout: Optional[float] = None):
        return WebDriverWait(self.driver, timeout or self.DEFAULT_TIMEOUT)

    @staticmethod
    def __filter_enabled_elements(elements: list[WebElement]):
        return [elem for elem in elements if elem.is_enabled()]
