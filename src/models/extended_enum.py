from enum import Enum

from pydash import snake_case


class ExtendedEnum(Enum):
    @classmethod
    def from_matched_name(cls, name: str):
        members = cls.__members__
        return members.get(snake_case(name).upper(), members.get(name.upper()))
