# Installation

### Requires

python3.9+

# Configuration

## Pycharm

Due to relative paths to files in static pytest config, to run/debug tests manually, set working directory as project root folder as run/debug configuration template for **Python tests** -> **Autodetect** and **pytest**

# Running tests for mBank task

In root directory:
```shell
pytest tests/test_arena/test_m_bank_task.py
```

Default test config is present in test directory (config.yaml). It is also possible to define driver settings via cmd:
* --browser "Chrome"
* --version "newest"

