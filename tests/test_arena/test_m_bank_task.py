from datetime import datetime

import pytest
from assertpy import assert_that
from parameterized import parameterized

from src.data.enums import PriorityType
from src.models.test_cases import TestArenaTestCase
from src.pages.test_arena.app.home_page import HomePage
from src.pages.test_arena.app.tasks.add_task_page import AddTaskPage, Task
from src.pages.test_arena.app.tasks_page import TasksPage
from src.pages.test_arena.demo_page import DemoPage
from src.pages.test_arena.login_page import LoginPage
from src.utils.date_time import get_hex_from_time_now


class MBankTaskTest(TestArenaTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.demo_page = DemoPage()
        cls.login_page = LoginPage()
        cls.home_page = HomePage()
        cls.tasks_page = TasksPage()
        cls.add_task_page = AddTaskPage()

    @pytest.mark.dependency()
    def test_1_login_to_demo_account(self):
        self.demo_page.navigate()
        self.demo_page.click_on_demo_button()
        self.demo_page.switch_to_next_window()
        self.page.wait_until_url_to_be(self.login_page.URL)
        self.login_page.login(*self.accounts.demo)
        assert_that(
            self.home_page.header.is_user_info_visible()
        ).is_true()
        assert_that(
            self.home_page.header.get_user_email()
        ).is_equal_to(self.accounts.demo.email)

    @pytest.mark.dependency(depends=["MBankTaskTest::test_1_login_to_demo_account"])
    def test_2_add_task_to_project(self):
        project = "api_v"
        task = Task(
            title=f"mBank task {get_hex_from_time_now()}",
            description="Test description \n with enter \t and tab",
            release_name="Wydanie 2",
            environments=["Środowisko Testowe"],
            versions=["Wersja 2"],
            priority=PriorityType.CRITICAL,
            due_date=datetime(2020, 3, 1, hour=6, minute=55),
            user_assigned="me",
            tags=["logowanie2"]
        )

        self.home_page.header.select_project(project)
        self.home_page.menu.click_on_tasks_tab()
        self.tasks_page.click_on_add_task_button()
        self.add_task_page.add_task(task)
        assert_that(
            self.add_task_page.is_alert_about_adding_task_visible()
        ).is_true()
        self.add_task_page.close_alert()
        self.add_task_page.click_back_button()
        self.tasks_page.search(task.title)
        assert_that(
            self.tasks_page.is_task_visible(task.title)
        ).is_true()

    # todo: add detailed task verification

    @parameterized.expand(
        [
            ("administrator@testarena.pl", "test"),
            ("invalid@testarena.pl", "sumXQQ72$L"),
        ]
    )
    @pytest.mark.dependency(depends=["MBankTaskTest::test_1_login_to_demo_account"])
    def test_3_invalid_login(self, email, password):
        current_url = self.driver.current_url
        if not (current_url.__contains__("zaloguj") or current_url.__contains__("logowanie")):
            self.tasks_page.header.click_on_logout_button()
        self.login_page.login(email, password)
        assert_that(
            self.login_page.get_login_form_error()
        ).is_not_none()
        self.login_page.refresh()
