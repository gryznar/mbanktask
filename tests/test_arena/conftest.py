import pytest
from _pytest.fixtures import SubRequest
from selenium.webdriver import Chrome

from src.data.enums import WebDriverType
from src.models.driver_container import WebDriverContainer
from src.models.test_cases import WebTestCase
from src.paths import Paths
from tests.test_arena.config import DefaultTestConfig


def pytest_addoption(parser):
    parser.addoption(
        "--browser", action="store", default=None, help="name of browser (Chrome, Firefox)",
    )
    parser.addoption(
        "--browser-version", action="store", default=None, help="version of browser"
    )


@pytest.fixture(scope="class", autouse=True)
def init_driver(request: SubRequest):
    if WebTestCase not in request.cls.mro():
        return
    cmd_config = request.config
    default_config = DefaultTestConfig().web_driver

    cmd_browser = cmd_config.getoption("browser")
    browser = WebDriverType.from_matched_name(cmd_browser) if cmd_browser else default_config.type
    browser_version = cmd_config.getoption("browser_version") or default_config.version
    if browser == WebDriverType.CHROME:
        if browser_version.lower() in ("latest", "newest"):
            driver = Chrome(Paths().chrome_driver_latest)
        else:
            raise NotImplementedError(f"not supported version: '{browser_version}'")
        driver.maximize_window()
    else:
        raise NotImplementedError(f"not supported browser: '{browser}'")
    WebDriverContainer().insert(driver)
    try:
        yield driver
    finally:
        driver.quit()
