from dataclasses import dataclass

from pytest_testconfig import config

from src.data.enums import WebDriverType


@dataclass
class WebDriverConfig:
    type: WebDriverType
    version: str


class DefaultTestConfig:
    def __init__(self, config_name: str = None):
        self.__config = config[config_name or "default"]

    @property
    def web_driver(self) -> WebDriverConfig:
        conf = self.__config["web_driver"]
        driver_type = conf["type"]
        return WebDriverConfig(
            type=WebDriverType.from_matched_name(driver_type),
            version=conf["version"]
        )
