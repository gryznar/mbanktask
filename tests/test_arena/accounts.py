from collections import namedtuple

from src.paths import Paths
from src.utils.files import read_yaml


class Accounts:
    __account = namedtuple("account", ["email", "password"])

    def __init__(self):
        self.__config = read_yaml(Paths.TEST_ARENA_TESTS_ACCOUNTS)

    @property
    def demo(self) -> __account:
        return self.__get_credentials("demo")

    def __get_credentials(self, account: str):
        conf = self.__config[account]
        return self.__account(conf["email"], conf["pass"])
